terraform {
    backend "s3" {
        region = "us-east-1"
        bucket = "terraform-jenkins-aws"
        encrypt = "true"
        key = "terraform.tfstate"
    }
